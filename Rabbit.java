import java.util.Random;

public class Rabbit{
	int jumpHeight;
	String name;
	int maxPoopsPerDay;
	
	public Rabbit(int jumpHeight, String name, int maxPoopsPerDay){
		this.jumpHeight = jumpHeight;
		this.name = name;
		this.maxPoopsPerDay = maxPoopsPerDay;
	}
	
	public int poopToday(){
		Random rand = new Random();
		return rand.nextInt(maxPoopsPerDay);
	}
	
	public int lastJump(){
		Random rand = new Random();
		return rand.nextInt(jumpHeight);
	}
}